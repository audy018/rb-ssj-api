/// <reference path="../../typings.d.ts" />

import * as express from 'express';
import * as crypto from 'crypto';
import * as moment from 'moment';

import { Router, Request, Response } from 'express';
import * as HttpStatus from 'http-status-codes';

import { SurveilModel } from '../models/surveil';
import { UserModel } from '../models/user';
import { LineModel } from '../models/line';

const surveilModel = new SurveilModel();
const userModel = new UserModel();
const lineModel = new LineModel();

const router: Router = Router();

router.get('/history', async (req: Request, res: Response) => {
  const hospcode = req.decoded.hospcode;
  const db: any = req.db;

  try {
    const rs: any = await surveilModel.getHistoryList(db, hospcode);
    res.send({ ok: true, rows: rs });
  } catch (error) {
    res.send({ ok: false, error: 'เกิดข้อผิดพลาด [500]' })
  }
});
// /surveil/
router.post('/', async (req: Request, res: Response) => {
  const db: any = req.db;
  const patientInfo: any = req.body.patientInfo;
  const surveil: any = req.body.surveil;
  const labs: any = req.body.labs;
  const screen: any = req.body.screen;

  const _patientInfo: any = {};
  _patientInfo.hospcode = patientInfo.hospcode;
  _patientInfo.cid = patientInfo.cid;
  _patientInfo.hn = patientInfo.hn;
  _patientInfo.pt_name = patientInfo.pt_name;
  _patientInfo.sex = patientInfo.sex;
  _patientInfo.birthday = patientInfo.birthday;
  _patientInfo.address = patientInfo.address;
  _patientInfo.addrpart = patientInfo.addrpart;
  _patientInfo.moopart = patientInfo.moopart;
  _patientInfo.tmbpart = patientInfo.tmbpart;
  _patientInfo.amppart = patientInfo.amppart;
  _patientInfo.chwpart = patientInfo.chwpart;
  _patientInfo.tel = patientInfo.tel;
  _patientInfo.user_id_import = req.decoded.id;

  const _surveil: any = {};

  _surveil.hospcode = surveil.hospcode;
  _surveil.hn = surveil.hn;
  _surveil.sv_number = surveil.sv_number;
  _surveil.vn = surveil.vn;
  _surveil.pdx = surveil.pdx;
  _surveil.vstdate = surveil.vstdate;
  _surveil.report_date = surveil.report_date;
  _surveil.begin_date = surveil.begin_date;
  _surveil.icd_name = surveil.icd_name;
  _surveil.name506 = surveil.name506;
  _surveil.code506 = surveil.code506;
  _surveil.cc = surveil.cc;
  _surveil.pe = surveil.pe;
  _surveil.ill_addr = surveil.ill_addr;
  _surveil.ill_moo = surveil.ill_moo;
  _surveil.ill_tmbpart = surveil.ill_tmbpart;
  _surveil.ill_amppart = surveil.ill_amppart;
  _surveil.ill_chwpart = surveil.ill_chwpart;
  _surveil.diag_dangue = surveil.diag_dangue;
  _surveil.time_send = moment().format('YYYY-MM-DD HH:mm:ss');
  _surveil.user_id = req.decoded.id;

  try {
    const rsDup: any = await surveilModel.checkDuplicatedPatient(db, _patientInfo.hospcode, _patientInfo.hn);
    if (rsDup.length) {
      const _info: any = {};
      _info.pt_name = patientInfo.pt_name;
      _info.sex = patientInfo.sex;
      _info.birthday = patientInfo.birthday;
      _info.address = patientInfo.address;
      _info.addrpart = patientInfo.addrpart;
      _info.moopart = patientInfo.moopart;
      _info.tmbpart = patientInfo.tmbpart;
      _info.amppart = patientInfo.amppart;
      _info.chwpart = patientInfo.chwpart;
      _info.tel = patientInfo.tel;
      _info.user_id_import = req.decoded.id;

      await surveilModel.updatePatientInfo(db, _patientInfo.hospcode, _patientInfo.hn, _info)
    } else {
      await surveilModel.savePatientInfo(db, _patientInfo);
    }

    // surveil
    let surveilId: any;

    const rsSurveilDup: any = await surveilModel.checkDuplicatedSurveil(db, _surveil.hospcode, _surveil.hn, _surveil.sv_number);
    if (rsSurveilDup.length) {

      surveilId = rsSurveilDup[0].surveil_id;

      const _surveilUpdate: any = {};
      _surveilUpdate.vn = surveil.vn;
      _surveilUpdate.pdx = surveil.pdx;
      _surveilUpdate.vstdate = surveil.vstdate;
      _surveilUpdate.report_date = surveil.report_date;
      _surveilUpdate.begin_date = surveil.begin_date;
      _surveilUpdate.icd_name = surveil.icd_name;
      _surveilUpdate.name506 = surveil.name506;
      _surveilUpdate.code506 = surveil.code506;
      _surveilUpdate.cc = surveil.cc;
      _surveilUpdate.pe = surveil.pe;
      _surveilUpdate.ill_addr = surveil.ill_addr;
      _surveilUpdate.ill_moo = surveil.ill_moo;
      _surveilUpdate.ill_tmbpart = surveil.ill_tmbpart;
      _surveilUpdate.ill_amppart = surveil.ill_amppart;
      _surveilUpdate.ill_chwpart = surveil.ill_chwpart;
      _surveilUpdate.diag_dangue = surveil.diag_dangue;
      _surveilUpdate.time_send = moment().format('YYYY-MM-DD HH:mm:ss');
      _surveilUpdate.user_id = req.decoded.id;

      await surveilModel.updateSurveil(db, _surveil.hospcode, _surveil.hn, _surveil.sv_number, _surveilUpdate);

    } else {
      const rsSurveilId = await surveilModel.saveSurveil(db, _surveil);
      surveilId = rsSurveilId[0];
    }

    // lab
    const labItems: any = [];

    if (labs.length) {
      labs.forEach(v => {
        const obj: any = {};
        obj.surveil_id = surveilId;
        obj.hospcode = surveil.hospcode;
        obj.hn = v.hn;
        obj.vn = v.vn;
        obj.lab_order_number = v.lab_order_number;
        obj.order_date = v.order_date;
        obj.order_time = v.order_time;
        obj.lab_items_name_ref = v.lab_items_name_ref;
        obj.lab_order_result = v.lab_order_result;
        obj.lab_items_normal_value = v.lab_items_normal_value;
        obj.lab_items_unit = v.lab_items_unit;

        labItems.push(obj);
      });
      // remove
      await surveilModel.removeLab(db, surveilId);
      // save
      await surveilModel.saveLab(db, labItems);
    }

    // screen
    const _screen: any = {};

    const rsScreenDup: any = await surveilModel.checkDuplicatedScreen(db, surveilId);

    _screen.sv_number = screen.sv_number;
    _screen.vn = screen.vn;
    _screen.screen_fever = screen.screen_fever;
    _screen.screen_headache = screen.screen_headache;
    _screen.screen_eye = screen.screen_eye;
    _screen.screen_muscle = screen.screen_muscle;
    _screen.screen_ortho = screen.screen_ortho;
    _screen.screen_rash = screen.screen_rash;
    _screen.screen_skin = screen.screen_skin;
    _screen.screen_liver = screen.screen_liver;
    _screen.screen_shock = screen.screen_shock;
    _screen.screen_tourniquet = screen.screen_tourniquet;

    if (rsScreenDup.length) {
      await surveilModel.updateScreen(db, surveilId, _screen);
    } else {
      _screen.surveil_id = surveilId;
      await surveilModel.saveScreen(db, _screen);
    }

    res.send({ ok: true, });

  } catch (error) {
    console.log(error);
    res.send({ ok: false, error: 'เกิดข้อผิดพลาด [500]' });
  }
});

router.post('/send', async (req: Request, res: Response) => {
  const db: any = req.db;
  const userId = req.decoded.id;
  const hospcode = req.decoded.hospcode;
  const surveilId = req.body.surveilId;
  const areaCode = req.decoded.areaCode;
  const illAmppart = req.body.illAmppart;

  let responseHospcode = '00000';
  let ssjHospcode = '00054';

  let lineIds = [];

  if (illAmppart === areaCode) {
    // ดึงข้อมูล สสอ.
    const rsAmp: any = await userModel.checkMasterCode(db, hospcode);

    if (rsAmp.length) {
      responseHospcode = rsAmp[0].hamp;
      const rsAmpLine: any = await userModel.getLineIds(db, responseHospcode);
      rsAmpLine.forEach(v => {
        if (v.id_line) {
          lineIds.push({ id: v.id, idLine: v.id_line, hospcode: v.hospcode });
        }
      });

      const rsSSJLine: any = await userModel.getLineIds(db, ssjHospcode);
      rsSSJLine.forEach(v => {
        if (v.id_line) {
          lineIds.push({ id: v.id, idLine: v.id_line, hospcode: v.hospcode });
        }
      });
    }
  } else {
    const rsSSJLine: any = await userModel.getLineIds(db, ssjHospcode);
    rsSSJLine.forEach(v => {
      if (v.id_line) {
        lineIds.push({ id: v.id, idLine: v.id_line, hospcode: v.hospcode });
      }
    });
  }

  // patient info
  const rsInfo: any = await surveilModel.getSurveilInfo(db, surveilId);
  const info = rsInfo.length ? rsInfo[0] : {};

  for (const v of lineIds) {
    const message = {
      "type": "flex",
      "altText": "แจ้งข้อมูลระบาด",
      "contents": {
        "type": "bubble",
        "body": {
          "type": "box",
          "layout": "vertical",
          "contents": [
            {
              "type": "text",
              "text": info.hospname,
              "wrap": true,
              "weight": "bold"
            },
            {
              "type": "text",
              "text": `ชื่อผู้ป่วย ${info.pt_name}`,
              "wrap": true
            },
            {
              "type": "text",
              "text": `โรคทางระบาด: ${info.name506}`,
              "wrap": true
            }
          ]
        },
        "footer": {
          "type": "box",
          "layout": "horizontal",
          "contents": [
            {
              "type": "button",
              "style": "primary",
              "action": {
                "type": "uri",
                "label": "รับทราบ",
                "uri": `${process.env.API_LINK}/line/accept?idLine=${v.idLine}&hospcode=${v.hospcode}&userId=${v.id}&surveilId=${surveilId}`
              }
            }
          ]
        }
      }
    };
    lineModel.pushMessage(v.idLine, [message]);
  }

  // save log

  for (const item of lineIds) {
    const sendDate = moment().format('YYYY-MM-DD HH:mm:ss');
    await surveilModel.saveSendLog(db, surveilId, userId, hospcode, item.hospcode, sendDate);
  }

  // console.log(lineIds);
  // console.log(JSON.stringify(message));

  res.send({ ok: true });
});

router.put('/', async (req: Request, res: Response) => {

});

router.delete('/', async (req: Request, res: Response) => {

});

export default router;

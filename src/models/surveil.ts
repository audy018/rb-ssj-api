import * as knex from 'knex';

export class SurveilModel {

  saveSurveil(db: knex, surveil: any) {
    return db('patient_surveil')
      .insert(surveil, 'surveil_id');
  }

  saveLab(db: knex, labs: any[]) {
    return db('patient_lab')
      .insert(labs);
  }

  saveScreen(db: knex, screen: any) {
    return db('patient_screen')
      .insert(screen);
  }

  updateScreen(db: knex, surveilId: any, screen: any) {
    return db('patient_screen')
      .where('surveil_id', surveilId)
      .update(screen);
  }

  checkDuplicatedScreen(db: knex, surveilId: any) {
    return db('patient_screen')
      .where('surveil_id', surveilId);
  }

  removeLab(db: knex, surveilId: any) {
    return db('patient_lab')
      .where('surveil_id', surveilId)
      .del();
  }

  updateSurveil(db: knex, hospcode: any, hn: any, svNumber: any, surveil: any) {
    return db('patient_surveil')
      .where('hospcode', hospcode)
      .where('sv_number', svNumber)
      .where('hn', hn)
      .update(surveil);
  }

  checkDuplicatedSurveil(db: knex, hospcode: any, hn: any, svNumber: any) {
    return db('patient_surveil')
      .select('surveil_id')
      .where('hospcode', hospcode)
      .where('sv_number', svNumber)
      .where('hn', hn);
  }

  savePatientInfo(db: knex, patient: any) {
    return db('patient_info')
      .insert(patient);
  }

  updatePatientInfo(db: knex, hospcode: any, hn: any, patient: any) {
    return db('patient_info')
      .where('hospcode', hospcode)
      .where('hn', hn)
      .update(patient);
  }

  checkDuplicatedPatient(db: knex, hospcode: any, hn: any) {
    return db('patient_info')
      .where('hospcode', hospcode)
      .where('hn', hn);
  }

  getHistoryList(db: knex, hospcode: any) {

    const sql = db('surveil_log')
      .select('surveil_id');

    return db('patient_surveil as ps')
      .select('ps.*', 'p.pt_name', 'p.birthday', 'p.cid', 'p.sex')
      .joinRaw('inner join patient_info as p on p.hospcode=ps.hospcode and p.hn=ps.hn')
      .where('ps.hospcode', hospcode)
      .whereNotIn('ps.surveil_id', sql)
      .orderBy('ps.vstdate', 'desc')
      .limit(100);
  }

  getSurveilInfo(db: knex, surveilId: any) {
    return db('patient_surveil as ps')
      .select('ps.*', 'p.pt_name', 'p.birthday', 'p.cid', 'p.sex', 'mh.hmainname as hospname')
      .joinRaw('inner join patient_info as p on p.hospcode=ps.hospcode and p.hn=ps.hn')
      .leftJoin('master_hospcode as mh', 'mh.hmain', 'ps.hospcode')
      .where('ps.surveil_id', surveilId)
      .limit(1);
  }

  saveSendLog(db: knex, surveilId: any,
    userId: any,
    fromHospcode: any,
    toHospcode: any,
    sendDate: any) {
    return db('surveil_log')
      .insert({
        surveil_id: surveilId,
        user_send: userId,
        send_from_hospcode: fromHospcode,
        send_to_hospcode: toHospcode,
        send_date: sendDate
      });
  }

  checkReplyStatus(db: knex, surveilId: any, hospcode: any) {
    return db('surveil_log as l')
      .select('l.*', 'u.name as user_fullname')
      .innerJoin('users as u', 'u.id', 'l.respond_user')
      .where('l.send_to_hospcode', hospcode)
      .where('l.surveil_id', surveilId)
      .where('l.status', 'Y');
  }

  checkSendLogInfo(db: knex, surveilId: any) {
    return db('surveil_log as l')
      .select('l.*', 'pi.pt_name', 'pt.name506')
      .innerJoin('patient_surveil as pt', 'pt.surveil_id', 'l.surveil_id')
      .joinRaw('inner join patient_info as pi on pi.hn=pt.hn and pi.hospcode=pt.hospcode')
      .where('l.surveil_id', surveilId);
  }

  updateReplyStatus(db: knex, surveilId: any, status: any,
    hospcode: any, responseUserId: any, responseTime: any) {
    return db('surveil_log')
      .where('surveil_id', surveilId)
      // .where('send_to_hospcode', hospcode)
      .update({
        status: status,
        respond_user: responseUserId,
        respond_time: responseTime
      })
  }

}
import * as knex from 'knex';

export class UserModel {

  doLogin(db: knex, username: any, password: any) {
    return db('users')
      .select('id', 'name', 'level', 'hospcode', 'amphur')
      .where('login_name', username)
      .where('login_password', password);
  }

  saveLineId(db: knex, username: any, lineId: any) {
    return db('users')
      .update('id_line', lineId)
      .where('login_name', username);
  }

  checkRegisteredLine(db: knex, lineId: any) {
    return db('users')
      .where('id_line', lineId);
  }

  getLineIds(db: knex, hospcode: any) {
    return db('users')
      .select('id_line', 'id', 'hospcode')
      .where('hospcode', hospcode);
  }

  getUserInfo(db: knex, userId: any) {
    return db('users')
      .select('id_line', 'id', 'hospcode')
      .where('id', userId);
  }

  checkMasterCode(db: knex, hospcode: any) {
    return db('master_hospcode')
      .where('hmain', hospcode);
  }
}